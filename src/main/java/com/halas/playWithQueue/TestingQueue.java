package com.halas.playWithQueue;

import java.util.*;

public class TestingQueue {



    public static void playWithQueue(){
        System.out.println("****PRIORITY QUEUE****");
        Queue<String> priorityQueue=new PriorityQueue<>();
        priorityQueue.offer("Helena");
        priorityQueue.offer("Bran");
        priorityQueue.offer("Edgar");
        priorityQueue.offer("Cent");
        priorityQueue.offer("Andrew");
        //priorityQueue.offer(null);//NullPointerException

        System.out.println("Queue: "+priorityQueue);
        System.out.println("poll: "+priorityQueue.poll());
        System.out.println("Queue: "+priorityQueue);
        System.out.println("peek: "+priorityQueue.peek());
        System.out.println("element: "+priorityQueue.element());
        System.out.println("remove: "+priorityQueue.remove());
        System.out.println("Queue: "+priorityQueue);

        System.out.print("Print Queue with iterator: ");
        Iterator<String> it=priorityQueue.iterator();
        while(it.hasNext()){
            System.out.print(it.next()+"  ");
        }
        System.out.println("\n");


        System.out.println("****ARRAY DEQUE****");
        ArrayDeque<String> arrayDeque=new ArrayDeque<>(6);
        arrayDeque.add("Second");
        arrayDeque.addFirst("First");
        arrayDeque.push("Third");
        arrayDeque.addFirst("Zero");
        arrayDeque.offer("element");

        System.out.println("ArrayDeque: "+arrayDeque);
        System.out.println("poll: "+arrayDeque.poll());
        System.out.println("ArrayDeque: "+arrayDeque);
        System.out.println("pollLast: "+arrayDeque.pollLast());
        System.out.println("POP: "+arrayDeque.pop());
        System.out.println("ArrayDeque: "+arrayDeque);
        System.out.println("peek: "+arrayDeque.peek());
        arrayDeque.offer("Zero");
        System.out.println("Zero has, but delete zero:"+arrayDeque.remove("zero"));
        System.out.println("ArrayDeque"+arrayDeque);

        System.out.print("print with iterator: ");
        Iterator<String> deIt=arrayDeque.iterator();
        while(deIt.hasNext()){
            System.out.print(deIt.next()+"  ");
        }

        System.out.println("\n\n");




    }
}
