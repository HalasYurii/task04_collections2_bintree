package com.halas;

import com.halas.BInTree.RunBinTree;
import com.halas.Enums.EnumsRun;
import com.halas.playWithList.TestingList;
import com.halas.playWithMap.TestingMap;
import com.halas.playWithQueue.TestingQueue;
import com.halas.playWithSet.TestingSet;
import com.halas.playWithString.MyString;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner input = new Scanner(System.in);

    public Main() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - task with Enums");
        menu.put("2", " 2 - task with Binary Tree");
        menu.put("3", " 3 - playing on String's");
        menu.put("4", " 4 - playing on Queue and Deque");
        menu.put("5", " 5 - playing on List");
        menu.put("6", " 6 - playing on Set");
        menu.put("7", " 7 - playing on Map");
        menu.put("0", " 0 - Exit");

        methodsMenu.put("1", EnumsRun::runEnum);
        methodsMenu.put("2", RunBinTree::runTree);
        methodsMenu.put("3", MyString::runPlayWithString);
        methodsMenu.put("4", TestingQueue::playWithQueue);
        methodsMenu.put("5", TestingList::playWithList);
        methodsMenu.put("6", TestingSet::playWithSet);
        methodsMenu.put("7", TestingMap::playWithMap);
    }

    private void showMenu() {
        for (String object : menu.values()) {
            System.out.println(object);
        }
    }

    public static void main(String[] args) {
        Main main = new Main();
        String keyMenu;
        while (true) {
            main.showMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine();
            System.out.println("\n\n");
            try {
                main.methodsMenu.get(keyMenu).start();
            } catch (Exception e) {
                if (keyMenu.equals("0")) {
                    break;
                } else {
                    System.out.println("Error, please try again.\n");
                }
                e.printStackTrace();
            }
        }
    }
}
