package com.halas.Enums;

public enum Days {
    SUNDAY(7, "Sun", "Domenica", "Domingo"),
    MONDAY(1, "Moon", "Lunedi", "Lunes"),
    TUESDAY(2, "Mars", "Martedi", "Martes"),
    WEDNESDAY(3, "Mercury", "Mercoledi", "Miércoles"),
    THURSDAY(4, "Jupiter", "Giovedi", "Jueves"),
    FRIDAY(5, "Venus", "Venerdi", "Viernes"),
    SATURDAY(6, "Saturn", "Sabato", "Sábado");
    private int num;
    //italian cameo bracelet representing the days of the week
    //corresponding to the planets as Roman gods:
    //Diana as the Moon for Monday,Mars for Tuesday etc.
    private String nameMoon;
    //name of the day byItalian
    private String byItalian;
    private String bySpanish;

    Days(int num, String namePlanet, String byItalian, String bySpanish) {
        this.num = num;
        this.nameMoon = namePlanet;
        this.byItalian = byItalian;
        this.bySpanish = bySpanish;
    }

    public int getNum() {
        return num;
    }

    public String getNamePlanet() {
        return nameMoon;
    }

    public String getByItalian() {
        return byItalian;
    }

    public String getBySpanish() {
        return bySpanish;
    }
}
