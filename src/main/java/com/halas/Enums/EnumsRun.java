package com.halas.Enums;

import com.halas.Functional;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class EnumsRun {
    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner input = new Scanner(System.in);


    public EnumsRun() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - Enum with days");
        menu.put("2", " 2 - Enum with basic operation");
        menu.put("3", " 3 - Exit");

        methodsMenu.put("1", this::daysExample);
        methodsMenu.put("2", this::operationExample);
    }

    private void showMenu() {
        for (String obj : menu.values()) {
            System.out.println(obj);
        }
    }

    private void daysExample() {
        System.out.println("\nMonday by spanish: " + Days.MONDAY.getBySpanish());
        System.out.println("Tuesday associated with planet: " + Days.TUESDAY.getNamePlanet());
        System.out.println("Wednesday get number: " + Days.WEDNESDAY.getNum());
        System.out.println("Thursday by spanish: " + Days.THURSDAY.getBySpanish());
        System.out.println("Friday associated with planet: " + Days.FRIDAY.getNamePlanet());
        System.out.println("Saturday by italian: " + Days.SATURDAY.getByItalian());
        System.out.println("Sunday by italian: " + Days.SUNDAY.getByItalian());

    }

    private void operationExample() {
        final int a = 6;
        final int b = 3;
        System.out.println("\na = " + a + " , b = " + b);
        System.out.println("a + b = " + Operation.ADDITION.total(a, b));
        System.out.println("a - b = " + Operation.SUBSTRACTION.total(a, b));
        System.out.println("a * b = " + Operation.MULTIPLICATION.total(a, b));
        System.out.println("a / b = " + Operation.DIVISION.total(a, b));
    }

    public static void runEnum() {
        String keyMenu;
        EnumsRun file = new EnumsRun();
        while (true) {
            file.showMenu();
            System.out.println("Please select menu point.");
            keyMenu = input.nextLine();
            System.out.println("\n\n");
            try {
                file.methodsMenu.get(keyMenu).start();
            } catch (Exception e) {
                if (keyMenu.equals("3")) {
                    break;
                } else {
                    System.out.println("Error, please try again.\n");
                }
            }
        }
    }
}
