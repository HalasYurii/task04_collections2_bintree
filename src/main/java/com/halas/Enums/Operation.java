package com.halas.Enums;

public enum Operation {
    ADDITION {
        @Override
        public double total(double a, double b) {
            return a + b;
        }
    },
    SUBSTRACTION {
        @Override
        public double total(double a, double b) {
            return a - b;
        }
    },
    DIVISION {
        @Override
        public double total(double a, double b) {
            return a / b;
        }
    },
    MULTIPLICATION {
        @Override
        public double total(double a, double b) {
            return a * b;
        }
    };

    public abstract double total(double a, double b);
}
