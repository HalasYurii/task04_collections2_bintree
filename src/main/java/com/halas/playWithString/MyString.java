package com.halas.playWithString;

public class MyString {
    public static void runPlayWithString(){
        String[] first = new String[]{"qwe", "QWE"};

        System.out.println("\"q\" equals \"Q\": "+first[0].equals(first[1]));
        System.out.println("\"qwe\" index 2: "+first[0].charAt(2));
        System.out.println("\"qwe\" contains \"QWE\": "+first[0].contains(first[1]));
        System.out.println("\"QWE\" contains \"qwe\": "+first[1].contains(first[0]));
        System.out.println("\""+first[0]+"\" compare to \""+first[1]+"\": "
                +first[0].compareTo(first[1]));
        System.out.println("\""+first[0]+"\" compare to Ignore Case\""
                +first[1]+"\": " +first[0].compareToIgnoreCase(first[1]));


        String a1="Dog";
        String a2=a1;
        System.out.println("a2 = a1 = "+a1);
        a2 = "Cat";
        System.out.println("a2 changed to \"Cat\", a1 = "+a1);
        a1=" Hello Yura Halas";
        a2="Hello pro man!";
        System.out.println("\""+a1+"\".substring(5,9): "+a1.substring(5,9));
        System.out.println("\""+a1+"\".substring(5): "+a1.substring(5));
        System.out.println("\""+a1+"\".concat("+a2+"): "+a1.concat(a2));
        System.out.println("\""+a1+"\".endWith(\"Halas\"): "+a1.endsWith("Halas"));
        //System.out.println("\""+a2+"\".repeat(2): "+a2.repeat(2));
        System.out.println("\""+a1+"\".split(\"Yura\"): "+a1.split("Yura"));
        //System.out.println("\""+a1+"\".strip(): "+a1.strip());
        //System.out.println("\""+a1+"\".stripLeading(): "+a1.stripLeading());
        //System.out.println("\""+a1+"\".stripTrailing(): "+a1.stripTrailing());
        System.out.println("\""+a1+"\".trim(): "+a1.trim());

    }
}
