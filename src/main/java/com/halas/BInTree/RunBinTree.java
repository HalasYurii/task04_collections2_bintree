package com.halas.BInTree;

import java.util.Iterator;

public class RunBinTree {
    public static void runTree(){
        MyTree<Integer,Integer> myTree=new MyTree<>();

        myTree.put(2,3);
        myTree.put(3,4);
        myTree.put(4,5);
        Iterator<Integer> itV= myTree.valueIterator();
        Iterator<Integer> itK=myTree.keyIterator();

        while(itK.hasNext()){
            System.out.print("Key: "+itK.next());
            System.out.println(",  value: "+itV.next());
        }


        System.out.println("\n\n");
    }
}
