package com.halas.BInTree;

import java.util.*;

public class MyTree<K,V> {

    private final Comparator<? super K> comparator;

    private transient Entry<K,V> root;

    private transient int size = 0;


    public MyTree() {
        comparator = null;
    }



    public V put(K key, V value) {
        MyTree.Entry<K,V> t = root;
        if (t == null) {
            compare(key, key); // type (and possibly null) check

            root = new MyTree.Entry<>(key, value, null);
            size = 1;
            return null;
        }
        int cmp;
        MyTree.Entry<K,V> parent;
        // split comparator and comparable paths
        Comparator<? super K> cpr = comparator;
        if (cpr != null) {
            do {
                parent = t;
                cmp = cpr.compare(key, t.key);
                if (cmp < 0)
                    t = t.left;
                else if (cmp > 0)
                    t = t.right;
                else
                    return t.setValue(value);
            } while (t != null);
        }
        else {
            if (key == null)
                throw new NullPointerException();
            @SuppressWarnings("unchecked")
            Comparable<? super K> k = (Comparable<? super K>) key;
            do {
                parent = t;
                cmp = k.compareTo(t.key);
                if (cmp < 0)
                    t = t.left;
                else if (cmp > 0)
                    t = t.right;
                else
                    return t.setValue(value);
            } while (t != null);
        }
        MyTree.Entry<K,V> e = new MyTree.Entry<>(key, value, parent);
        if (cmp < 0)
            parent.left = e;
        else
            parent.right = e;
        size++;
        return null;
    }

    final MyTree.Entry<K,V> getEntryUsingComparator(Object key) {
        @SuppressWarnings("unchecked")
        K k = (K) key;
        Comparator<? super K> cpr = comparator;
        if (cpr != null) {
            MyTree.Entry<K,V> p = root;
            while (p != null) {
                int cmp = cpr.compare(k, p.getKey());
                if (cmp < 0)
                    p = p.left;
                else if (cmp > 0)
                    p = p.right;
                else
                    return p;
            }
        }
        return null;
    }


    final int compare(Object k1, Object k2) {
        return comparator==null ? ((Comparable<? super K>)k1).compareTo((K)k2)
                : comparator.compare((K)k1, (K)k2);
    }

    static final boolean valEquals(Object o1, Object o2) {
        return (o1==null ? o2==null : o1.equals(o2));
    }


    final class KeyIterator extends PrivateEntryIterator<K> {
        KeyIterator(Entry<K,V> first) {
            super(first);
        }
        public K next() {
            return nextEntry().key;
        }
    }

    final class ValueIterator extends PrivateEntryIterator<V> {
        ValueIterator(MyTree.Entry<K,V> first) {
            super(first);
        }
        public V next() {
            return nextEntry().value;
        }
    }
    public Iterator<V> valueIterator() {
        return new MyTree.ValueIterator(getFirstEntry());
    }




    static final class Entry<K,V> implements Map.Entry<K,V> {
        K key;
        V value;
        MyTree.Entry<K,V> left;
        MyTree.Entry<K,V> right;
        MyTree.Entry<K,V> parent;

        Entry(K key, V value, MyTree.Entry<K,V> parent) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }

        public boolean equals(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry<?,?> e = (Map.Entry<?,?>)o;

            return valEquals(key,e.getKey()) && valEquals(value,e.getValue());
        }

        public int hashCode() {
            int keyHash = (key==null ? 0 : key.hashCode());
            int valueHash = (value==null ? 0 : value.hashCode());
            return keyHash ^ valueHash;
        }

        public String toString() {
            return key + "=" + value;
        }
    }




    abstract class PrivateEntryIterator<T> implements Iterator<T> {
        MyTree.Entry<K,V> next;
        MyTree.Entry<K,V> lastReturned;

        PrivateEntryIterator(MyTree.Entry<K,V> first) {
            lastReturned = null;
            next = first;
        }

        public final boolean hasNext() {
            return next != null;
        }

        public final MyTree.Entry<K,V> nextEntry() {
            MyTree.Entry<K,V> e = next;
            if (e == null)
                throw new NoSuchElementException();
            next = successor(e);
            lastReturned = e;
            return e;
        }
    }

    public Iterator<K> keyIterator() {
        return new KeyIterator(getFirstEntry());
    }

    final MyTree.Entry<K,V> getFirstEntry() {
        MyTree.Entry<K,V> p = root;
        if (p != null)
            while (p.left != null)
                p = p.left;
        return p;
    }





    static <K,V> MyTree.Entry<K,V> successor(MyTree.Entry<K,V> t) {
        if (t == null)
            return null;
        else if (t.right != null) {
            MyTree.Entry<K,V> p = t.right;
            while (p.left != null)
                p = p.left;
            return p;
        } else {
            MyTree.Entry<K,V> p = t.parent;
            MyTree.Entry<K,V> ch = t;
            while (p != null && ch == p.right) {
                ch = p;
                p = p.parent;
            }
            return p;
        }
    }


}

