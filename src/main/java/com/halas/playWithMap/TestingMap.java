package com.halas.playWithMap;

import java.util.*;

public class TestingMap {
    public static void playWithMap(){

        System.out.println("****HashMap****");
        System.out.println("_<Integer,String>_");
        HashMap<Integer,String> hashMap=new HashMap<>();

        hashMap.put(2,"SECOND_S");
        // if FINE -> return null ; ELSE return VALUE with key associated with previous key
        System.out.println("add element: "+hashMap.put(2,"Second"));

        hashMap.put(null,"000");// true/false
        hashMap.put(3,null);// true/false

        //only unique key, but values can be similar!!
        System.out.println("Your hashMap:  "+hashMap);

        System.out.println("put if Absent, expect - null:  "+hashMap.putIfAbsent(4,"Second"));
        System.out.println("getOrDefault, expect - second: "+hashMap.getOrDefault(2,"default_2"));
        System.out.println("getOrDefault, expect - default_1:  "+hashMap.getOrDefault(1,"default_1"));

        // If OK return value, if NOT_OK return null
        System.out.println("remove with key 2, expect null: "+hashMap.remove(5));

        // true/false
        System.out.println("remove with key and value, expect true: "+hashMap.remove(2,"Second"));
        System.out.println("Your hashMap:  "+hashMap);


        System.out.println("\n\n");


        System.out.println("****TREE MAP****");
        System.out.println("_<String,Integer>_");

        TreeMap<String,Integer>  treeMap=new TreeMap<>();

        //return value
        System.out.println("add, expect null: "+treeMap.put("first",1));

        //return value
        System.out.println("add, expect null: "+treeMap.put("third",3));

        //return value
        System.out.println("add, expect first: "+treeMap.put("first",11));

        //return value
        System.out.println("add, expect null: "+treeMap.put("second",2));

        //return value
        System.out.println("add, expect null: "+treeMap.put("second",null));//VALUE CAN = NULL

        //System.out.println("add, expect null: "+treeMap.put(null,2));//CAN'T KEY = NULL
        //System.out.println("add, expect null: "+treeMap.put(null,null));//CAN'T KEY = NULL

        System.out.println("Your TreeMap: "+treeMap);

        System.out.println("poll first Entry:  "+treeMap.pollFirstEntry());
        System.out.println("remove, expect value:  "+treeMap.remove("second"));
        System.out.println("remove, expect false:  "+treeMap.remove("FIRST",1));

        System.out.println("Your treeMap: "+treeMap);
        System.out.println("\n\n");


        System.out.println("****LINKED HASH MAP****");
        System.out.println("_<String,String>_");

        LinkedHashMap<String,String> linkedHasMap=new LinkedHashMap<>();

        System.out.println("add, expect null: "+linkedHasMap.put("One","First"));
        System.out.println("add, expect null: "+linkedHasMap.put("Four","Fourth"));
        System.out.println("add, expect null: "+linkedHasMap.put("Three","Third"));

        linkedHasMap.put("vvv",null);//Fine
        linkedHasMap.put(null,"Value");//Fine
        linkedHasMap.put(null,null);//Fine

        System.out.println("Your linkedHashMap:"+linkedHasMap);

        System.out.println("remove, expect Third: "+linkedHasMap.remove("Three"));
        System.out.println("remove, expect false: "+linkedHasMap.remove("One","first"));

        System.out.println("Your linkedHashMap:"+linkedHasMap);
    }
}
