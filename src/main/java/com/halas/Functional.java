package com.halas;

@FunctionalInterface
public interface Functional {
    void start();
}
