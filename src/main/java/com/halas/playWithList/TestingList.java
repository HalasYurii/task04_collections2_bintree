package com.halas.playWithList;

import java.util.ArrayList;
import java.util.LinkedList;

public class TestingList {


    public static void playWithList() {


        System.out.println("****ARRAY LIST****");
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(2);//fine
        arrayList.add(0, null);//IndexOutOfBoundsException
        arrayList.set(1, 0);//IndexOutOfBoundException

        arrayList.remove(1);//IndexOutOfBoundsException
        arrayList.remove(new Integer(0));//fine

        arrayList.get(0);//IndexOutOfBoundsException

        System.out.println("Array list: " + arrayList);
        //for add element:
        // --add(index,value) від 0 до останнього проініціалізованого
        // елемента або наступний, але на +2 throw IndexOutOfBoundException
        // --add(value)/ addLast(value) / addFirst(value) true/false
        // --set(index,value) etc. throw IndexOutOfBoundException
        // offer(value) / offerFirst(value) / offerLast(value) true/false
        //push(value) true/false .. add to START
        System.out.println("\n\n****LINKED LIST****");
        LinkedList<Integer> linkedList = new LinkedList<Integer>();

        //add
        linkedList.add(0, 10);
        linkedList.add(1, 12);
        linkedList.add(1, 9);
        linkedList.addFirst(1);
        linkedList.offer(null);
        linkedList.push(2);


        //remove
        linkedList.poll();//true/false
        linkedList.pollFirst();//true/false
        linkedList.pollLast();//true/false

        linkedList.pop();//IndexOutOfBoundException delete from START!
        linkedList.remove(1);//IndexOutOfBoundException
        linkedList.remove();//IndexOutOfBoundException

        linkedList.remove(new Integer(2));//true/false
        linkedList.removeFirstOccurrence(2);//true/false


        //get
        linkedList.get(1);//IndexOutOfBoundsException
        linkedList.element();//NoSuchElementException
        linkedList.getFirst();//NoSuchElementException
        linkedList.getLast();//NoSuchElementException
        linkedList.peek();//fine!
        linkedList.peekFirst();//fine!
        linkedList.peekLast();//fine!

        System.out.println(linkedList);


    }
}
