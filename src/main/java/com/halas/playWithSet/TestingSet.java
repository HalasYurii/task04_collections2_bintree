package com.halas.playWithSet;

import com.halas.Enums.Days;

import java.util.*;

public class TestingSet {
    public static void playWithSet(){
        System.out.println("****TREE SET****");
        System.out.println("_<ENUM>_");
        TreeSet<Days> days= new TreeSet<>();
        days.add(Days.MONDAY);//ClassCastException, NullPointerException
        days.add(Days.TUESDAY);//ClassCastException, NullPointerException
        days.add(Days.FRIDAY);//ClassCastException, NullPointerException
        days.add(Days.SUNDAY);//ClassCastException, NullPointerException
        days.add(Days.SATURDAY);//ClassCastException, NullPointerException
        //days.add(null); //NullPointerException!!!

        System.out.println("poll Last: "+days.pollLast());//Fine
        System.out.println("poll First: "+days.pollFirst());//Fine

        //ClassCastException, NullPointerException
        System.out.println("remove tuesday: "+days.remove(Days.TUESDAY));

        //ClassCastException, NullPointerException
        System.out.println("Function ceiling: "+days.ceiling(Days.SUNDAY));

        System.out.print("Print with iterator:   ");
        Iterator<Days> itS=days.iterator();
        while (itS.hasNext()){
            System.out.print(itS.next()+"  ");
        }
        System.out.print("\n\nPrint with descending iterator:   ");
        Iterator<Days> itD=days.descendingIterator();
        while (itD.hasNext()){
            System.out.print(itD.next()+"  ");
        }
        System.out.println("\n\n");

        //NoSuchElementException
        System.out.println("Function first(): "+days.first());

        //ClassCastException,NullPointerException
        System.out.println("Function floor for WEDNESDAY: "+days.floor(Days.WEDNESDAY));

        //ClassCastException, NullPointerException
        System.out.println("lower THURSDAY"+days.lower(Days.THURSDAY));

        //ClassCastException, NullPointerException
        System.out.println("higher THURSDAY"+days.higher(Days.MONDAY));

        //IllegalArgumentException, ClassCastException,NullPointerException
        System.out.println("headSet FRIDAY: "+days.headSet(Days.FRIDAY));

        //IllegalArgumentException, NullPointerException, ClassCastException
        System.out.println("tailSet MONDAY: "+days.tailSet(Days.MONDAY));


        System.out.println("\n\n****HASH SET****");
        System.out.println("_<Integer>_\n");
        HashSet<Integer> value=new HashSet<>();

        //add value without Exceptions!
        value.add(2);//fine added to the end
        value.add(3);//fine added to the end
        value.add(10);//fine added to the end

        //fine
        System.out.println("remove by value \"2\" :"+value.remove(2));//remove by value without Exceptions!

        //without Exceptions
        System.out.println("(i expect that will be \"true\")if contains 3: "+value.contains(3));//fine

        //withoutExceptions
        System.out.println("(i expect that false) if is empty: "+value.isEmpty());//fine

        //without Exceptions
        value.clear();//fine

        //withoutExceptions
        System.out.println("(i expect that true, before i used .clear()) if is empty: "+value.isEmpty());//fine


        System.out.println("\n\n****LINKED HASH SET*****");
        System.out.println("_<STRING>_");
        LinkedHashSet<String> linkedHashSet=new LinkedHashSet<>();

        LinkedHashSet<String> forReationAll=new LinkedHashSet<>();

        linkedHashSet.add("First");//fine added withoutExceptions!
        linkedHashSet.add("Second");//without Exceptions
        linkedHashSet.add("Third");
        linkedHashSet.add("Fourth");

        forReationAll.add("Second");
        forReationAll.add("Fifth");

        System.out.println("remove element \"Third\" expect that will be true: "
                +linkedHashSet.remove("Third"));//true\false without Exceptions

        System.out.println("\n\nFirst Collection: "+linkedHashSet);
        System.out.println("Second Collection: "+forReationAll);
        System.out.println("First.retainAll(Second):  "
                + linkedHashSet.retainAll(forReationAll));
        System.out.println("First: "+linkedHashSet);
    }
}
